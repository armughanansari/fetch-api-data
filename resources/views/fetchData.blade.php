<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<div id="app">


    <h1>Counciltaxfinder.com</h1>
    <table class="table table-striped">
        <tr>
                <td> Address: {{$DataContent[0]['Address']}}</td>
                <td> Council Name: {{$DataContent[0]['Council Name']}}</td>
                <td> Band: {{$DataContent[0]['Band']}}</td>
                <td> Improvement Indicator: {{$DataContent[0]['Improvement Indicator']}}</td>
                <td> Local Authority Reference Number: {{$DataContent[0]['Local Authority Reference Number']}}</td>
                <td> Tax: {{$DataContent[0]['Tax']}}</td>
                <td> Tax Monthly: {{$DataContent[0]['Tax Monthly']}}</td>
                <td> Year: {{$DataContent[0]['Year']}}</td>
                <td> Council Web: {{$DataContent[0]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[1]['Address']}}</td>
            <td> Council Name: {{$DataContent[1]['Council Name']}}</td>
            <td> Band: {{$DataContent[1]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[1]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[1]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[1]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[1]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[1]['Year']}}</td>
            <td> Council Web: {{$DataContent[1]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[2]['Address']}}</td>
            <td> Council Name: {{$DataContent[2]['Council Name']}}</td>
            <td> Band: {{$DataContent[2]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[2]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[2]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[2]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[2]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[2]['Year']}}</td>
            <td> Council Web: {{$DataContent[2]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[3]['Address']}}</td>
            <td> Council Name: {{$DataContent[3]['Council Name']}}</td>
            <td> Band: {{$DataContent[3]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[3]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[3]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[3]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[3]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[3]['Year']}}</td>
            <td> Council Web: {{$DataContent[3]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[4]['Address']}}</td>
            <td> Council Name: {{$DataContent[4]['Council Name']}}</td>
            <td> Band: {{$DataContent[4]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[4]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[4]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[4]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[4]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[4]['Year']}}</td>
            <td> Council Web: {{$DataContent[4]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[5]['Address']}}</td>
            <td> Council Name: {{$DataContent[5]['Council Name']}}</td>
            <td> Band: {{$DataContent[5]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[5]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[5]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[5]['Tax']}}</td>
            <td> Tax: Monthly {{$DataContent[5]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[5]['Year']}}</td>
            <td> Council Web: {{$DataContent[5]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[6]['Address']}}</td>
            <td> Council Name: {{$DataContent[6]['Council Name']}}</td>
            <td> Band: {{$DataContent[6]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[6]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[6]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[6]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[6]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[6]['Year']}}</td>
            <td> Council Web: {{$DataContent[6]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[7]['Address']}}</td>
            <td> Council Name: {{$DataContent[7]['Council Name']}}</td>
            <td> Band: {{$DataContent[7]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[7]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[7]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[7]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[7]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[7]['Year']}}</td>
            <td> Council Web: {{$DataContent[7]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address {{$DataContent[8]['Address']}}</td>
            <td> Council Name {{$DataContent[8]['Council Name']}}</td>
            <td> Band {{$DataContent[8]['Band']}}</td>
            <td> Improvement Indicator {{$DataContent[8]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number {{$DataContent[8]['Local Authority Reference Number']}}</td>
            <td> Tax {{$DataContent[8]['Tax']}}</td>
            <td> Tax Monthly {{$DataContent[8]['Tax Monthly']}}</td>
            <td> Year {{$DataContent[8]['Year']}}</td>
            <td> Council Web {{$DataContent[8]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[9]['Address']}}</td>
            <td> Council Name: {{$DataContent[9]['Council Name']}}</td>
            <td> Band: {{$DataContent[9]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[9]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[9]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[9]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[9]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[9]['Year']}}</td>
            <td> Council Web: {{$DataContent[9]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[10]['Address']}}</td>
            <td> Council Name: {{$DataContent[10]['Council Name']}}</td>
            <td> Band: {{$DataContent[10]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[10]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[10]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[10]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[10]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[10]['Year']}}</td>
            <td> Council Web: {{$DataContent[10]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[11]['Address']}}</td>
            <td> Council Name: {{$DataContent[11]['Council Name']}}</td>
            <td> Band: {{$DataContent[11]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[11]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[11]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[11]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[11]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[11]['Year']}}</td>
            <td> Council Web: {{$DataContent[11]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[12]['Address']}}</td>
            <td> Council Name: {{$DataContent[12]['Council Name']}}</td>
            <td> Band: {{$DataContent[12]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[12]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[12]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[12]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[12]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[12]['Year']}}</td>
            <td> Council Web: {{$DataContent[12]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[13]['Address']}}</td>
            <td> Council Name: {{$DataContent[13]['Council Name']}}</td>
            <td> Band: {{$DataContent[13]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[13]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[13]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[13]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[13]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[13]['Year']}}</td>
            <td> Council Web: {{$DataContent[13]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[14]['Address']}}</td>
            <td> Council Name: {{$DataContent[14]['Council Name']}}</td>
            <td> Band: {{$DataContent[14]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[14]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[14]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[14]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[14]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[14]['Year']}}</td>
            <td> Council Web: {{$DataContent[14]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[15]['Address']}}</td>
            <td> Council Name: {{$DataContent[15]['Council Name']}}</td>
            <td> Band: {{$DataContent[15]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[15]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[15]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[15]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[15]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[15]['Year']}}</td>
            <td> Council Web: {{$DataContent[15]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[16]['Address']}}</td>
            <td> Council Name: {{$DataContent[16]['Council Name']}}</td>
            <td> Band: {{$DataContent[16]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[16]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[16]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[16]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[16]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[16]['Year']}}</td>
            <td> Council Web: {{$DataContent[16]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[17]['Address']}}</td>
            <td> Council Name: {{$DataContent[17]['Council Name']}}</td>
            <td> Band: {{$DataContent[17]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[17]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[17]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[17]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[17]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[17]['Year']}}</td>
            <td> Council Web: {{$DataContent[17]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[18]['Address']}}</td>
            <td> Council Name: {{$DataContent[18]['Council Name']}}</td>
            <td> Band: {{$DataContent[18]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[18]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number" {{$DataContent[18]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[18]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[18]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[18]['Year']}}</td>
            <td> Council Web: {{$DataContent[18]['Council Web']}}</td>
        </tr>
        <tr>
            <td> Address: {{$DataContent[19]['Address']}}</td>
            <td> Council Name: {{$DataContent[19]['Council Name']}}</td>
            <td> Band: {{$DataContent[19]['Band']}}</td>
            <td> Improvement Indicator: {{$DataContent[19]['Improvement Indicator']}}</td>
            <td> Local Authority Reference Number: {{$DataContent[19]['Local Authority Reference Number']}}</td>
            <td> Tax: {{$DataContent[19]['Tax']}}</td>
            <td> Tax Monthly: {{$DataContent[19]['Tax Monthly']}}</td>
            <td> Year: {{$DataContent[19]['Year']}}</td>
            <td> Council Web: {{$DataContent[19]['Council Web']}}</td>
        </tr>
    </table>
    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>


