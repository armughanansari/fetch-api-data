<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FetchController extends Controller
{
    public function index()
    {
        $endpoint = "https://api.counciltaxfinder.com/counciltaxfinder/counciltax/KW8%206JS";
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $endpoint, ['query' => [
            'door' => "",
            'page' => "",
            'userid' => 106,
            'apikey' => 2365
        ]]);

        $statusCode = $response->getStatusCode();
        $DataContent = json_decode($response->getBody(), true);


        return view('fetchData' , compact('DataContent'));

    }
}
